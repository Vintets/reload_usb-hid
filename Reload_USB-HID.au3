#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=usb.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_Res_Comment=Reload_USB-HID
#AutoIt3Wrapper_Res_Fileversion=1.0.1
#AutoIt3Wrapper_Res_LegalCopyright=Vint
#AutoIt3Wrapper_Res_requestedExecutionLevel=None
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

;===============================================================================
;
; Description:      Reload_USB-HID
; Version:          1.0.1
; Requirement(s):   Autoit 3.3.8.1
; Author(s):        Vint
;
;===============================================================================

#Region    ************ Includes ************
#include <Array.au3>
#include <WinAPI.au3>
#EndRegion ************ Includes ************

Opt('MustDeclareVars', 1)
;Opt('TrayIconDebug', 1)

Global $wndName = '��������� ���������'
Global $TreeView = '[CLASS:SysTreeView32]'
Global $numUSB = '1'

Main()

Func Main()
    Local $hWnd, $PID, $AllPunkt, $punkt

    _RunningMMC()
    $hWnd = WinGetHandle($wndName)
    $PID = WinGetProcess($wndName)
    ConsoleWrite($wndName & ':  HWND ' & $hWnd  & '  ������������� PID ' & $PID & @CRLF)
    ;MsgBox(4096, '', $wndName & @CRLF & 'HWND ' & $hWnd & @CRLF & '������������� PID ' & $PID, 3)

    _SelectUSB($hWnd, $punkt)
    _Properties($punkt)
    WinClose($hWnd)
EndFunc   ;==>Main

Func _Properties($punkt)
    Local $hWnd_Properties, $OnOff, $hContrOK, $hContrGO
    Local $hContrOnOff = '[CLASS:ComboBox]'

    WinWait('��������: ' & $punkt)
    $hWnd_Properties = WinGetHandle('��������: ' & $punkt)
    ConsoleWrite('HWND �������� ' & $hWnd_Properties & @CRLF)

    ;Local $hControl = ControlGetHandle($hWnd_Properties, '', $hContrOnOff)
    ;ConsoleWrite('���������� c������: ' & $hControl & @CRLF)
    ;$OnOff = ControlCommand($hWnd_Properties, '', $hContrOnOff, 'FindString', '��� ���������� �� ������������ (���������)')
    ;ConsoleWrite('����� ���/���� ' & $OnOff & @CRLF)
    ControlCommand($hWnd_Properties, '', $hContrOnOff, 'SetCurrentSelection', 1)  ; ����
    
    $hContrGO = ControlGetHandle($hWnd_Properties, '', '[CLASS:Button; TEXT:���&������]')
    ConsoleWrite('���������: ' & $hContrGO & @CRLF)
    ControlClick($hWnd_Properties, '', $hContrGO, 'main')
    Sleep(2000)

    ControlCommand($hWnd_Properties, '', $hContrOnOff, 'SetCurrentSelection', 0)  ; ���
    
    $hContrOK = ControlGetHandle($hWnd_Properties, '', '[CLASS:Button; TEXT:��]') ; INSTANCE:3]
    ConsoleWrite('OK: ' & $hContrOK & @CRLF)

    ControlClick($hWnd_Properties, '', $hContrOK, 'main')
    Sleep(2000)
EndFunc   ;==>_Properties

Func _SelectUSB($_hWnd, $_punkt)
    Local $hControl

    $hControl = ControlGetHandle($_hWnd, '', $TreeView)
    ControlFocus($_hWnd, '', $TreeView)
    ConsoleWrite('���������� �������� SysTreeView: ' & $hControl & @CRLF)

    ;$AllPunkt = ControlTreeView($_hWnd, '', $TreeView, 'GetItemCount', '#0|����������� ������������� ���������������� ���� USB')
    ;ConsoleWrite('������� ' & $AllPunkt & @CRLF)
    ;MsgBox(4096, '', '������� ' & $AllPunkt, 0)

    $_punkt = ControlTreeView($_hWnd, '', $TreeView, 'GetText', '#0|����������� ������������� ���������������� ���� USB|#' & $numUSB)
    ConsoleWrite('����� ' & $_punkt & @CRLF)

    ControlTreeView($_hWnd, '', $TreeView, 'Select', '#0|����������� ������������� ���������������� ���� USB|#' & $numUSB)
    Send('{ENTER}')
EndFunc   ;==>_SelectUSB

Func _RunningMMC()  ; ��������� ��������� ���������
    ;~ '[CLASS:MMCMainFrame; TITLE:��������� ���������]'
    If Not WinExists($wndName) Then
        ShellExecute('devmgmt.msc')
    EndIf
    WinWait($wndName)
EndFunc   ;==>_RunningMMC

